from django.urls import path

from . import views

app_name = 'core'
urlpatterns = [
    path('movies', views.MovieList.as_view(), name='MovieList'),  # as_view() method used to call class based view
    path('people', views.PersonList.as_view(), name='PersonList'),
    path('movie/<int:pk>', views.MovieDetail.as_view(), name='MovieDetail'),
    path('person/<int:pk>', views.PersonDetail.as_view(), name='PersonDetail'),
    path('movie/<int:movie_id>/vote', views.CreateVote.as_view(), name='CreateVote'),
    path('movie/<int:movie_id>/vote/<int:pk>', views.UpdateVote.as_view(), name='UpdateVote'),
    path('movie/<int:movie_id/image/upload', views.MovieImageUpload.as_view(), name='MovieImageUpload'),
    path('top_10_movies', views.TopMovies.as_view(), name='TopMovies'),
]
